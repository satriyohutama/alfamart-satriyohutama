import React from 'react';
import './App.css';
import { Form, Input, Button , Table} from "antd";
import Axios from "axios";

function App() {
  return (
    <div className="App">
    <h1> Guest BOOK</h1>
    </div>
  );
}
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};
var config = {
  headers: {'Access-Control-Allow-Origin': '*'}
};

  

export const Login = () => {
  const onFinish = (values) => {
    Axios({
        method: 'post',
        url: 'http://localhost:8080/api/v1/guestbook',
        data: {
          ...values
        },
        config
      });
      setTimeout(function () {
        window.location.reload()
    }, 100);
  };
 

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div style={{marginRight: "200px"}}>
      <Form
        {...layout}
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Guest Name"
          name="guestName"
          rules={[
            {
              required: true,
              message: "Please input your Name!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Institution"
          name="institution"
          rules={[
            {
              required: true,
              message: "Please input your Institution!",
            },
          ]}
        >
          <Input/>
        </Form.Item>


        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
      
      
    </div>
  );
};

const columns = [
  {
    title: "id",
    dataIndex: "id",
  },
  {
    title: "Guest Name",
    dataIndex: "guestName",
  },
  {
    title: "Institution",
    dataIndex: "institution",
  },
  {
    title: "Created On",
    dataIndex: "createdOn",
  },
];

function onChange(pagination, filters, sorter, extra) {
  console.log("params", pagination, filters, sorter, extra);
}


export const TableView = () => {
  const [content, setContent] = React.useState([]);
 

  React.useEffect(() => {
    Axios({
      method: "get",
      url: "http://localhost:8080/api/v1/guestbook", config
    }).then(function (response) {
      if (response.data) {
        setContent(response.data);
      }
      if (response.err) {
        console.log(response.err);
      }
    });
  }, []);

    return <Table columns={columns} dataSource={content} onChange={onChange} />;
};

export default App;
