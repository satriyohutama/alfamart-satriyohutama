import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css'
import './index.css';
import App, {Login, TableView} from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <App />
    <Login/>
    <TableView/>
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
