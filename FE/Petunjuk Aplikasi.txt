Untuk merunning FE :
1. Open File Project FE 
2. New Terminal 
3. Perintah install module : npm install
3. Perintah run : npm start
4. Tunggu hingga secara otomatis Aplikasi terbuka di browser
5. Data pada aplikasi akan muncul ketika Service BE sedang dijalankan

Penggunaan Aplikasi
Ini merupakan aplikasi guestbook yang dapat membuat daftar diri dan dapat menampilkan data orang yang telah mendaftarkan atau menuliskan data dirinya. 

Langkah mendaftarkan/menuliskan data diri :
1. Setelah memasuki halaman aplikasi, masukkan input Guest Name dan Institution.
2. Kemudian submit.

Data yang didaftarkan/dituliskan akan ditampilkan pada table di halaman aplikasi.