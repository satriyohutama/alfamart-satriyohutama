package com.soalno2.guestbook.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="guestbook")
public class Guestbook {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "guest_name")
	private String guestName;
	
	@Column(name = "institution")
	private String institution;
	
	@Column(name = "created_on")
	private Timestamp createdOn;


	public Guestbook() {
		super();
	}

	public Guestbook(long id, String guestName, String institution, Timestamp createdOn) {
		super();
		this.id = id;
		this.guestName = guestName;
		this.institution = institution;
		this.createdOn = createdOn;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	
	
}
