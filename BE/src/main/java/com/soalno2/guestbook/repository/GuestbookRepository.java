package com.soalno2.guestbook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.soalno2.guestbook.model.Guestbook;

public interface GuestbookRepository extends JpaRepository<Guestbook, Long>{

}
