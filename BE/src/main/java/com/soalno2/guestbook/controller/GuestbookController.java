package com.soalno2.guestbook.controller;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.soalno2.guestbook.model.Guestbook;
import com.soalno2.guestbook.repository.GuestbookRepository;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class GuestbookController {

	@Autowired
	private GuestbookRepository guestbookRepository;
	
	@GetMapping("guestbook")
	public List<Guestbook> getAllGuestbooks(){
		return this.guestbookRepository.findAll();
	}

	
	@PostMapping("/guestbook")
	public Guestbook createGuestbook(@Valid @RequestBody Guestbook guestbook) {
		LocalDateTime localNow = LocalDateTime.now();  
		Timestamp dateNow = Timestamp.valueOf(localNow);
		guestbook.setCreatedOn(dateNow);
		return guestbookRepository.save(guestbook);
	}
}
