-- -----------------------------
-- Sequence structure for guestbook
-- -----------------------------
DROP SEQUENCE IF EXISTS "public"."guestbook_id_seq";
CREATE SEQUENCE "public"."guestbook_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for guestbook
-- ----------------------------
DROP TABLE IF EXISTS "public"."guestbook";
CREATE TABLE "public"."guestbook" (
  "id" int8 NOT NULL DEFAULT nextval('guestbook_id_seq'::regclass),
  "guest_name"  varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "institution" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_on" timestamp(6)
) 

INSERT INTO "guestbook"("id", "guest_name", "institution", "created_on") VALUES (1, 'Pak Dodi', 'PT Sinar Jaya Maju', '2020-06-06 14:35:40.009');
INSERT INTO "guestbook"("id", "guest_name", "institution", "created_on") VALUES (2, 'Joni', 'Universitas', '2020-06-06 14:37:42.705');
INSERT INTO "guestbook"("id", "guest_name", "institution", "created_on") VALUES (3, 'Rohayati', 'Universitas', '2020-06-06 14:38:01.642');