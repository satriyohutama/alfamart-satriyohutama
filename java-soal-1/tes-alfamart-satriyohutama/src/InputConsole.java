import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class InputConsole {
	public static void main(String[] args) {
		
		try (Scanner scanner = new Scanner(System.in)) {
			Random I  = new Random();
			List<Integer> list = new ArrayList<Integer>();
			int number; 
			int pick = 0;
			System.out.println("Pick a Number Between 10 - 20 ");
			pick = scanner.nextInt();
				//looping input banyak deret angka (10 - 20)
				while (pick>=21 || pick<=9) { 
				    System.out.println("Invalid a Number \"" + pick + "\". Please Try again");
				    System.out.println("Pick Number Between 10 - 20");
				    pick = scanner.nextInt();
	
				};
				//generate angka random (10 - 20)
				for(int i=1; i<=pick;i++){
				    number = I.nextInt(11) + 10;
				    list.add(number);
				}

			System.out.println("list : "+list);
			
			//min untuk most left
			int min = list.get(0);
			//max untuk most right
			int max = list.get(list.size()-1);
			//inisialisasi input user (pilihan angka oleh user)
			pick = 0;
			//inisialisai penjumlahan user dan system
			int AI = 0;
			int sumUser = 0;
			int sumAI = 0;
			
			System.out.println("Pick a Number from most left or right");
			pick = scanner.nextInt();
			
				while (pick != min && pick != max ) {
					System.out.println("Invalid a Number \"" + pick + "\". Please Try again");	
					System.out.println("Pick a Number from most left or right");
				    pick = scanner.nextInt();	
				};
			//system memilih data kanan jika user memilih most left dan data kiri jika user memilih most right
				if (pick == max) {
					AI = min;
				} else if (pick == min){
					AI = max;
				}	
			//penjumlahan pertama user dan system, kemudian number pada array dihapus
		    sumUser = sumUser + pick;
		    sumAI = sumAI + AI;
		    list.remove(0);
		    list.remove(list.size()-1);
			System.out.println("sum User : " + sumUser);
			System.out.println("sum AI : " + sumAI);
			System.out.println("list : "+list);
				
				while (list.size() != 1 && list.isEmpty() == false) {
					//min untuk most left
					min = list.get(0);
					//max untuk most right
					max = list.get(list.size()-1);
					pick = 0;
					
					System.out.println("Pick a Number from most left or right");
					pick = scanner.nextInt();
						while (pick != min && pick != max ) {
							System.out.println("Invalid a Number \"" + pick + "\". Please Try again");	
							System.out.println("Pick a Number from most left or right");
						    pick = scanner.nextInt();    
						};
						//system memilih data kanan jika user memilih most left dan data kiri jika user memilih most right
						if (pick == max) {
							AI = list.get(list.size()-2);
						    list.remove(list.size()-1);
						    list.remove(list.size()-1);
						} else if (pick == min){
							AI = list.get(1);
							list.remove(0);
							list.remove(0);
						}
						sumUser = sumUser + pick;
					    sumAI = sumAI + AI;
						System.out.println("sum User : " + sumUser);
						System.out.println("sum AI : " + sumAI);
						if(list.isEmpty() == true) {
						} else {
						System.out.println("list : "+list);
						}
				};
				if (sumUser>sumAI) {
					System.out.println("Game Over, You Win!");
				} else if (sumUser==sumAI) {
					System.out.println("Game Over, Draw Game!");
				} else 
					System.out.println("Game Over, You Lose!");
				

		}
    }
}